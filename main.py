import sys
import os
from PySide2 import QtXml
from PySide2.QtUiTools import QUiLoader
from PySide2.QtWidgets import QApplication, QPushButton, QLineEdit
from PySide2.QtCore import QFile
from botthread import BotThread

def spawn_bot():

    bot = BotThread(user.text(), password.text(),server.currentText())
    bot.start()


def set_server_list(comboBox):
    comboBox.addItem('ar')
    comboBox.addItem('br')
    comboBox.addItem('cz')
    comboBox.addItem('de')
    comboBox.addItem('en')
    comboBox.addItem('es')
    comboBox.addItem('fi')
    comboBox.addItem('fr')
    comboBox.addItem('gr')
    comboBox.addItem('hr')
    comboBox.addItem('hu')
    comboBox.addItem('it')
    comboBox.addItem('jp')
    comboBox.addItem('mx')
    comboBox.addItem('nl')
    comboBox.addItem('pl')
    comboBox.addItem('pt')
    comboBox.addItem('ro')
    comboBox.addItem('ru')
    comboBox.addItem('se')
    comboBox.addItem('si')
    comboBox.addItem('sk')
    comboBox.addItem('tr')
    comboBox.addItem('tw')
    comboBox.addItem('us')
    comboBox.addItem('yu')


if __name__ == "__main__":

    if getattr(sys, 'frozen', False):
        application_path = sys._MEIPASS + "\\"
    else:
        application_path = os.path.dirname(os.path.abspath(__file__)) + '/'

    app = QApplication(sys.argv)

    file = QFile(application_path + 'main.ui')
    file.open(QFile.ReadOnly)

    loader = QUiLoader()
    ui = loader.load(file)

    startButton = ui.pushButton_start
    startButton.clicked.connect(spawn_bot)

    user = ui.lineEdit_user
    password = ui.lineEdit_pass
    server = ui.comboBox_server
    set_server_list(server)

    ui.show()

    sys.exit(app.exec_())
